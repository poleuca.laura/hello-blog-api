using AutoMapper;
using hello_blog_api.Models;
using hello_blog_api.Repository;

public class MappingProfile : Profile 
{
   public MappingProfile()
   {
      CreateMap<BlogPost, BlogPostModel>().ReverseMap();
      CreateMap<BlogPost, BlogPostModelCreate>().ReverseMap();
   }
}